# README #

This is our Global Game Jam 2017 game entry's repository

![bardoo the bard wallpaper.png](https://bitbucket.org/repo/Ax7Bbz/images/3266602608-bardoo%20the%20bard%20wallpaper.png)

We are testing Unity's Collab service. This repository is a backup, just in case something goes wrong on the other repo.
Unity Online History: [Link](https://developer.cloud.unity3d.com/collab/orgs/caiomga_/projects/ggj20171/commits/)

Global Game Jam's Game Page: [Link](http://globalgamejam.org/2017/games/bardo-bard)

Devlog: [Link](http://imgur.com/a/sVQLc)

Download the game: [Link](https://drive.google.com/drive/folders/0BxHeOgaF8dW9NGR4QXo4XzhpRWs?usp=sharing)

### Bardo, the Bard ###
Defend yourself against the wild critters and ferocious monsters. Use the power of your music to turn the enemies away. Hit the Fairy Princess with your purple note to heal.

### Inspirations ###
* Global Game Jam 2017's theme: Waves
* Sound Waves
* Waves of enemies
* RPG Bards

### Features ###
* Dynamic SoundScape: Each enemy type has its own music. If there is at least one enemy of any given type on the screen its audio is played, otherwise not. This gives information to the player about the gamestate through the audio.
* Arcade style - It gets harder overtime and the game ends when the player is defeated
* Different note types with different effects
* All assets were made during the game jam (including sound)

### Team ###
* Caio Marchi - Team Manager & Programmer & Animator
* João Neto - Audio Owner & Composer
* Michelle Kimura - Lead Artist
* Lael Jader - Lead Programmer
* Rubens Pazinato - Game Designer & Programmer

### Platforms ###
* Windows 32-bits
* WIndows 64-bits
* WebGL