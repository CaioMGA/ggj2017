﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour {

    public Image splashScreenImage;
    public Text year;
    [Range(1, 500)]
    public int speedfade = 100;
    public float timeWait;
    public float timeToStartAndQuit = 1f;
    protected float lastTime;
    protected float currentcolor;
    protected int state;

    void Start() {
        splashScreenImage.color = Color.black;
        year.color = Color.black;
        lastTime = Time.time;
        state = 0;
    }

    void Update() {
        if (state == 0 && Time.time - lastTime >= timeToStartAndQuit) {
            state++;
        }

        if (state == 1) {
            currentcolor = splashScreenImage.color.r + ((Time.deltaTime * speedfade) / 100);
            if (currentcolor > 1) {
                currentcolor = 1;
                lastTime = Time.time;
                state++;
            }
            splashScreenImage.color = new Color(currentcolor, currentcolor, currentcolor, 1);
            year.color = new Color(currentcolor, currentcolor, currentcolor, 1);
        }
        if (state == 2 && Time.time - lastTime > timeWait) {
            lastTime = Time.time;
            state++;
        }
        if (state == 3) {
            currentcolor = splashScreenImage.color.r - ((Time.deltaTime * speedfade) / 100);
            if (currentcolor < 0) {
                currentcolor = 0;
                lastTime = Time.time;
                state++;
            }
            splashScreenImage.color = new Color(currentcolor, currentcolor, currentcolor, 1);
            year.color = new Color(currentcolor, currentcolor, currentcolor, 1);
        }

        if (state == 4 && Time.time - lastTime >= timeToStartAndQuit) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void Reset() {
        splashScreenImage.color = Color.black;
        year.color = Color.black;
        lastTime = Time.time;
        state = 0;
    }

}
