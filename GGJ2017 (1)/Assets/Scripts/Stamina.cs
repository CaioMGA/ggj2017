﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stamina : MonoBehaviour {

    public float stamina;

	// Use this for initialization
	void Start () {
        stamina = 100;
	}
	
	// Update is called once per frame
	void Update () {
		if (stamina < 100)
        {
            stamina++;
        }
	}
}
