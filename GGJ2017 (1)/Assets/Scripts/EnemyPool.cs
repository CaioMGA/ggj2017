﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour {

	public GameObject [] enemies; // enemies array
	public int enemy_count = 10; // number of enemies to be instantiated at game Start

	void Start () {
		foreach(GameObject go in enemies){ // for each enemy type
			GameObject enemy_type = new GameObject(); // instantiate a new enemy holder
			enemy_type.name = go.name + " pool";
			enemy_type.transform.parent = transform;
			for(int i = 0; i < enemy_count; i++){ //instantiate enemies
				GameObject new_enemy = Instantiate(go);
				new_enemy.transform.parent = enemy_type.transform; // set parent to newly created enemy holder
				new_enemy.SetActive(false);
			}
		}
	}
	
	public GameObject get_enemy(int type){
		GameObject enemy;
		GameObject enemies_holder = transform.GetChild(type).gameObject;
		for(int i = 0; i < enemy_count; i++){ // search for a enemy inactive
			enemy = enemies_holder.transform.GetChild(i).gameObject;
			if(enemy.activeInHierarchy == false){
				return enemy;
			}
		}
		Debug.Log("No enemies left");
		return null; // no enemies left to be deployed
	}
}
