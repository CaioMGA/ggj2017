﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePowerMeter : MonoBehaviour {

    public GameObject PowerBar;

    private float pos;
    private float powerInterval;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        powerInterval = PowerBar.GetComponent<ShootPower>().powerInterval;
        pos = (PowerBar.GetComponent<ShootPower>().power / powerInterval)/5;
        pos = pos - 0.1f;
        transform.localPosition = new Vector3(pos, 0, 0);
    }
}
