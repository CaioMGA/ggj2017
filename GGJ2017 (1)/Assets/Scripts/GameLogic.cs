﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour {
	public Transform [] enemy_deploys;
	public PrincessScript princess;
	public Transform princess_deploy;
	public EnemyPool enemy_pool;
    public float GameTime;
    public float DificultTimer1;
    public float DificultTimer2;

	public float enemy_deploy_interval;
	float enemy_deploy_deltatime = 0f; // time since last enemy deployed

	public float princess_deploy_interval = 10f;
	float princess_deploy_deltatime = 0f;

	int enemy_types_total = 0;
	int deploy_spots_total = 0;
	GameObject next_enemy;
	Health player_health;

	//states
	bool gameover = false;

	void Start(){
		enemy_types_total = enemy_pool.enemies.Length;
		deploy_spots_total = enemy_deploys.Length;
		player_health = GameObject.FindGameObjectWithTag("Bard").GetComponent<Health>();
        enemy_deploy_interval = 3f;

    }

	void Update(){
        if (GameTime < DificultTimer2)
        {
            GameTime += Time.deltaTime;
            if (GameTime >= DificultTimer2)
            {
                enemy_deploy_interval = 1f;
            }
            else if (GameTime >= DificultTimer1)
            {
                enemy_deploy_interval = 2f;
            }
        }
		if(gameover){
			// show game over screen
			SceneManager.LoadScene("Game Over");
		} else {
			update_enemy_deploy();
			monitor_player_health();
			update_princess_deploy();
		}
	}

	void update_enemy_deploy(){
		if(enemy_deploy_deltatime >= enemy_deploy_interval){
			send_next_enemy();
			enemy_deploy_deltatime = 0;
		} else {
			enemy_deploy_deltatime += Time.deltaTime;
		}
	}

	void update_princess_deploy(){
		if(princess_deploy_deltatime >= princess_deploy_interval){
			Debug.Log("Bardo Healed");
			princess.transform.position = princess_deploy.position;
			princess.gameObject.SetActive(true);
			princess.move();
			princess_deploy_deltatime = 0;
			princess_deploy_interval = Random.Range(15, 150);
		} else {
			princess_deploy_deltatime += Time.deltaTime;
		}
	}

	void send_next_enemy(){
		int next_enemy_type = Random.Range(0, enemy_types_total); //get random enemy type
		int next_enemy_deploy_spot = Random.Range(0, deploy_spots_total); //get random deploy position
		next_enemy = enemy_pool.get_enemy(next_enemy_type);
		if(next_enemy != null){
			next_enemy.transform.position = enemy_deploys[next_enemy_deploy_spot].position;
			next_enemy.GetComponent<EnemyScript>().deploy();
		}
	}

	void monitor_player_health(){
		if(player_health.currentHealth <= 0){
			gameover = true;
		}
	}
}
