﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    public GameObject Attack_BigWave;
    public GameObject Attack_MediumWave;
    public GameObject Attack_SmallWave;
    public GameObject Lullaby_BigWave;
    public GameObject Lullaby_MediumWave;
    public GameObject Lullaby_SmallWave;
    public GameObject Enchantment_BigWave;
    public GameObject Enchantment_MediumWave;
    public GameObject Enchantment_SmallWave;
    public GameObject Heal_BigWave;
    public GameObject Heal_MediumWave;
    public GameObject Heal_SmallWave;
    public GameObject PowerBar;
    public GameObject StaminaProprieties;

	public AudioClip ShootAcute;
	public AudioClip ShootMid;
	public AudioClip ShootBass;


    private float power;
    private int powerInterval;

    public void shot(float power)
    {
        power = PowerBar.GetComponent<ShootPower>().power;
        powerInterval = (int)PowerBar.GetComponent<ShootPower>().powerInterval;
        if (power >= 9 && power <= 11)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && StaminaProprieties.GetComponent<Stamina>().stamina > Attack_SmallWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Attack_SmallWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Attack_SmallWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && StaminaProprieties.GetComponent<Stamina>().stamina > Lullaby_SmallWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Lullaby_SmallWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Lullaby_SmallWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && StaminaProprieties.GetComponent<Stamina>().stamina > Enchantment_SmallWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Enchantment_SmallWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Enchantment_SmallWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && StaminaProprieties.GetComponent<Stamina>().stamina > Heal_SmallWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Heal_SmallWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Heal_SmallWave.GetComponent<NoteProprieties>().cost;
            }
			AudioSource audio = GetComponent<AudioSource>();
			audio.clip = ShootAcute;
			audio.Play();
        }
        else if (power > 5 && power < 15)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && StaminaProprieties.GetComponent<Stamina>().stamina > Attack_MediumWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Attack_MediumWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Attack_MediumWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && StaminaProprieties.GetComponent<Stamina>().stamina > Lullaby_MediumWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Lullaby_MediumWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Lullaby_MediumWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && StaminaProprieties.GetComponent<Stamina>().stamina > Enchantment_MediumWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Enchantment_MediumWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Enchantment_MediumWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && StaminaProprieties.GetComponent<Stamina>().stamina > Heal_MediumWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Heal_MediumWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Heal_MediumWave.GetComponent<NoteProprieties>().cost;
            }
			AudioSource audio = GetComponent<AudioSource>();
			audio.clip = ShootMid;
			audio.Play();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && StaminaProprieties.GetComponent<Stamina>().stamina > Attack_BigWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Attack_BigWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Attack_BigWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && StaminaProprieties.GetComponent<Stamina>().stamina > Lullaby_BigWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Lullaby_BigWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Lullaby_BigWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && StaminaProprieties.GetComponent<Stamina>().stamina > Enchantment_BigWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Enchantment_BigWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Enchantment_BigWave.GetComponent<NoteProprieties>().cost;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && StaminaProprieties.GetComponent<Stamina>().stamina > Heal_BigWave.GetComponent<NoteProprieties>().cost)
            {
                Instantiate(Heal_BigWave, transform.position, transform.rotation);
                StaminaProprieties.GetComponent<Stamina>().stamina -= Heal_BigWave.GetComponent<NoteProprieties>().cost;
            }
			AudioSource audio = GetComponent<AudioSource>();
			audio.clip = ShootBass;
			audio.Play();
        }
    }
}
