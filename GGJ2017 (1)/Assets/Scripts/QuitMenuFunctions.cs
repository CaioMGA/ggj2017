﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitMenuFunctions : MonoBehaviour {

    public GameObject quitCanvas;

    public void NoQuitBoxPess() {
        quitCanvas.SetActive(false);
    }
    public void YesQuitBoxPess() {
        Application.Quit();
    }
}
