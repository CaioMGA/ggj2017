﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHealthBar : MonoBehaviour {

    public Health health;
    public Image healthImageFill;

	void Update () {
        healthImageFill.fillAmount = health.currentHealth / 100f;

    }
}
