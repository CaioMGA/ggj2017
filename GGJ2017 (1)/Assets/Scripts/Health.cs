﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public int maxHealth;
    public int currentHealth;

    void Start() {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage) {
        if (currentHealth - damage < 0) {
            currentHealth = 0;
        }
        else {
            currentHealth -= damage;
        }
    }

    public void Heal(int value) {
        if (currentHealth + value > maxHealth) {
            currentHealth = maxHealth;
        }
        else {
            currentHealth += value;
        }
    }
}
