﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMenuButton : MonoBehaviour {


	public AudioSource source;

	void Start () {
		source = GetComponent<AudioSource> ();
	}

	public void Play (AudioClip clip) {
		source.clip = clip;
		source.Play();
	}
}
