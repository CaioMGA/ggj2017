﻿using UnityEngine;

/// <summary>
/// Configuracao dos botoes do joystick.
/// </summary>
public class KeyboardJoystick {

    public bool GetButtonDown(int index) {
        if (index == 0) {
            return Input.GetKeyDown(KeyCode.Alpha1);
        }
        if (index == 1) {
            return Input.GetKeyDown(KeyCode.Alpha2);
        }
        if (index == 2) {
            return Input.GetKeyDown(KeyCode.Alpha3);
        }
        if (index == 3) {
            return Input.GetKeyDown(KeyCode.Alpha4);
        }
        else {
            return false;
        }
    }

    public bool GetButton(int index) {
        if (index == 0) {
            return Input.GetKey(KeyCode.Alpha1);
        }
        if (index == 1) {
            return Input.GetKey(KeyCode.Alpha2);
        }
        if (index == 2) {
            return Input.GetKey(KeyCode.Alpha3);
        }
        if (index == 3) {
            return Input.GetKey(KeyCode.Alpha4);
        }
        else {
            return false;
        }
    }
    public bool GetButtonUp(int index) {
        if (index == 0) {
            return Input.GetKeyUp(KeyCode.Alpha1);
        }
        if (index == 1) {
            return Input.GetKeyUp(KeyCode.Alpha2);
        }
        if (index == 2) {
            return Input.GetKeyUp(KeyCode.Alpha3);
        }
        if (index == 3) {
            return Input.GetKeyUp(KeyCode.Alpha4);
        }
        else {
            return false;
        }
    }

    public bool GetAnyButton() {
        if (Input.GetKey(KeyCode.Alpha1)) {
              return true;
        }
        else if (Input.GetKey(KeyCode.Alpha2)) {
            return true;
        }
        else if (Input.GetKey(KeyCode.Alpha3)) {
            return true;
        }
        else if (Input.GetKey(KeyCode.Alpha4)) {
            return true;
        }
        else {
            return false;
        }
    }
}
