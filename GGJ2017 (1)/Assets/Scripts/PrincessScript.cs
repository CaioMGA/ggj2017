﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrincessScript : MonoBehaviour {
	public float speed = 2000f;
	public int  heal_points = 10;
	public GameObject heal_fx;
	Rigidbody2D rb;
	Vector2 walk_force;

	void Start(){
		rb = GetComponent<Rigidbody2D>();
		walk_force = new Vector2(-speed, 0);
	}

	void Awake () {
		//	move();
	}
	
	public void move(){
		rb.velocity = Vector2.zero;
		rb.AddRelativeForce(walk_force);
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.transform.CompareTag("Princess Destroyer")){
			gameObject.SetActive(false);
		}

		if(other.transform.CompareTag("HealNote")){
			heal_Bardo();
			gameObject.SetActive(false);
		}
	}

	public void heal_Bardo(){
		Debug.Log("Healed!");
		GameObject.FindGameObjectWithTag("Bard").GetComponent<Health>().Heal(heal_points);
		heal_fx.SetActive(false);
		heal_fx.SetActive(true);
	}
}
