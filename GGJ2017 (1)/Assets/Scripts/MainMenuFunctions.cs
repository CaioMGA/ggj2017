﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuFunctions : MonoBehaviour {

    public GameObject quitCanvas;
    public GameObject settingsCanvas;
    public GameObject creditsCanvas;

    public void NewGameMainMenuPress() {
        SceneManager.LoadScene("Main Game");
    }

    public void SettingsMainMenuPress() {
        settingsCanvas.SetActive(true);
    }

    public void CreditsMainMenuPress() {
        creditsCanvas.SetActive(true);
    }

    public void QuitMainMenuPress() {
        quitCanvas.SetActive(true);
    }
}
