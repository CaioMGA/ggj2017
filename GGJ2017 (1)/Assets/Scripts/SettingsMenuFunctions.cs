﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.IO;

public class SettingsMenuFunctions : MonoBehaviour {

    public GameObject settingsCanvas;
    public Slider musicVolume;
    public Slider SFXVolume;
    public AudioMixer musicMixer;
    public AudioMixer SFXMixer;
    //public AudioSource musicSource;
    //public AudioSource FXSource;
    public GameSettings gameSettings;

    public void Start() {
        gameSettings = new GameSettings();
        LoadSettings();
    }

    public void Update() {
        MusicVolumeChange();
        FXVolumeChange();
    }

    public void BackSettingsMenuPress() {
        settingsCanvas.SetActive(false);
    }

    public void MusicVolumeChange() {
        musicMixer.SetFloat("Master", gameSettings.musicVolume);
    }
    public void FXVolumeChange() {
        SFXMixer.SetFloat("Master", gameSettings.SFXVolume);
    }

    public void ApplySettingsMenuPress() {
        SaveSettings();
    }

    public void SaveSettings() {
        //musicSource.volume = gameSettings.FXVolume = FXVolume.value;
        //FXSource.volume = gameSettings.FXVolume = FXVolume.value;
        gameSettings.musicVolume = musicVolume.value;
        gameSettings.SFXVolume = SFXVolume.value;

        string jsonData = JsonUtility.ToJson(gameSettings, true);
        File.WriteAllText(Application.persistentDataPath + "/gamesettings.json", jsonData);
    }

    public void LoadSettings() {
        if (File.Exists(Application.persistentDataPath + "/gamesettings.json")) {
            gameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(Application.persistentDataPath + "/gamesettings.json"));
        }
        else {
            gameSettings = new GameSettings();
        }
            musicVolume.value = gameSettings.musicVolume;
            SFXVolume.value = gameSettings.SFXVolume;
    }
}
