﻿using UnityEngine;

/// <summary>
/// Configuracao dos botoes do mouse.
/// </summary>
public class MouseJoystick {

    public bool GetButtonDown(int index) {
        if (index == 0) {
            return Input.GetMouseButtonDown(0);
        }
        if (index == 1) {
            return Input.GetMouseButtonDown(1);
        }
        if (index == 2) {
            return Input.GetMouseButtonDown(2);
        }
        else {
            return false;
        }
    }

    public bool GetButton(int index) {
        if (index == 0) {
            return Input.GetMouseButton(0);
        }
        if (index == 1) {
            return Input.GetMouseButton(1);
        }
        if (index == 2) {
            return Input.GetMouseButton(2);
        }
        else {
            return false;
        }
    }
    public bool GetButtonUp(int index) {
        if (index == 0) {
            return Input.GetMouseButtonUp(0);
        }
        if (index == 1) {
            return Input.GetMouseButtonUp(1);
        }
        if (index == 2) {
            return Input.GetMouseButtonUp(2);
        }
        else {
            return false;
        }
    }
}
