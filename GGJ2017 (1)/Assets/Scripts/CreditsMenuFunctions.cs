﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsMenuFunctions : MonoBehaviour {

    public GameObject creditsCanvas;

    public void BackCreditsMenuPress() {
        creditsCanvas.SetActive(false);
    } 
}
