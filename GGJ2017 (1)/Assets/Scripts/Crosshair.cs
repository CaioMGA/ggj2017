﻿using UnityEngine;

/// <summary>
/// GUIImage seguir o mouse
/// </summary>
public class Crosshair : MonoBehaviour {

    public float smooth = 0.2f;
    protected RectTransform thisTransform;

    void Start() {
        thisTransform = GetComponent<RectTransform>();
    }

    void Update() {
        thisTransform.position = Vector2.Lerp(thisTransform.position, Input.mousePosition, smooth);
    }
}
