﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour {
	public RectTransform ingame_menu;
	public RectTransform quit_menu;

	void Start () {
		
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			if(Time.timeScale == 0){
				Time.timeScale = 1;
				hide_menu();
			} else {
				Time.timeScale = 0;
				show_menu();
			}
		}
	}

	public void show_menu(){
		ingame_menu.gameObject.SetActive(true);
	}
	public void hide_menu(){
		ingame_menu.gameObject.SetActive(false);
	}

	public void show_prompt_exit(){
		quit_menu.gameObject.SetActive(true);
	}

	public void hide_prompt(){
		quit_menu.gameObject.SetActive(false);
	}

	public void go_to_title_screen(){
		//load title screen
		//Debug.Log("Go to title screen");
        SceneManager.LoadScene("Title Screen");
    }

	public void restart(){
		//Debug.Log("Restart Scene");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

	public void resume(){
		Time.timeScale = 1;
		hide_menu();
	}
}
