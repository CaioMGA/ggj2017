﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class StageMusicScript : MonoBehaviour {

	public AudioSource [] channels;
	public bool [] muted_channels; // testing

	EnemyPool enemy_pool;
	int enemy_types_count;
	int enemies_instantiated_by_type;

	void Start(){
		enemy_pool = GameObject.FindGameObjectWithTag("Enemy Pool").GetComponent<EnemyPool>(); //find enemy pool
		enemy_types_count = enemy_pool.enemies.Length; // get number of enemy holders
		enemies_instantiated_by_type = enemy_pool.enemy_count;
		muted_channels = new bool[enemy_types_count]; 
		for(int i = 0; i < enemy_types_count; i++){
			muted_channels[i] = true;
		}
	}

	void Update(){
		monitor_enemies();
	}

	public void mute_channel(int channel){
		if(channels[channel].mute == false){
			channels[channel].mute = true;
			muted_channels[channel] = true;
			Debug.Log ("mute");
		}
	}

	public void unmute_channel(int channel){
		if(channels[channel].mute == true){
			channels[channel].mute = false;
			muted_channels[channel] = false;
		}
	}

	void monitor_enemies(){
		for(int i = 0; i< enemy_types_count; i++){ // checks every type of enemy
			Transform type = enemy_pool.transform.GetChild(i);
			bool enemies_active = false;
			for(int j = 0; j < enemies_instantiated_by_type; j++){ // checks every enemy created
				if(type.GetChild(j).gameObject.activeInHierarchy){
					// if there is at least one active enemy its channel should be on
					enemies_active = true;
					break; // no need to check any further
				}
			}
			if(enemies_active){
				unmute_channel(i);
			} else {
				mute_channel(i);

			}
		}
	}
}
