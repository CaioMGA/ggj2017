﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateStaminaBar : MonoBehaviour {

    public Stamina stamina;
    public Image staminaImageFill;

	void Update () {
        staminaImageFill.fillAmount = stamina.stamina /100f;

    }
}
