﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowDamage : MonoBehaviour {

    private Vector3 position;
    private Vector3 screenPointPosition;
    private Camera cameraHold;
    private string text;

	// Use this for initialization
	void Start () {
        cameraHold = Camera.main;
        screenPointPosition = cameraHold.WorldToScreenPoint(position);
	}
	
	// Update is called once per frame
	void Update () {
        screenPointPosition.y -= 1;
	}

    public static void DamageText (string text, Vector3 position)
    {
        var newInstance = new GameObject("Damage Popup");
        var damagePopup = newInstance.AddComponent<ShowDamage>();
        damagePopup.position = position;
        damagePopup.text = text;
    }

    private void OnGUI()
    {
        var screenPos = cameraHold.WorldToScreenPoint(position);
        GUI.Label(new Rect(screenPos.x, screenPointPosition.y, 50, 20), text);
        Destroy(gameObject, 0.5f);
    }
}
