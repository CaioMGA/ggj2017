﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPower : MonoBehaviour {

    public GameObject AimPlace;
    public float power = 0;
    public float powerInterval;

    private bool PowerPlus = true;

    private void Start()
    {
        powerInterval = 20;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Alpha4))
        {
            AimPlace.GetComponent<Shoot>().shot(power);
        }

        if (PowerPlus && power == powerInterval)
            PowerPlus = false;
        if (!PowerPlus && power == 0)
            PowerPlus = true;  

        if (PowerPlus)
            power += 1;
        else
            power -= 1;
    }
}