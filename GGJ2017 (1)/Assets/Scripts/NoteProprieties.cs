﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteProprieties : MonoBehaviour
{

    public int power;
    public bool isAttack;
    public bool isLullaby;
    public bool isEnchantment;
    public bool isHeal;
    public float cost;

    void OnTriggerEnter2D(Collider2D other)
    {
        {
            if (other.transform.CompareTag("Enemy Animal"))
            {
                if (isAttack)
                {
                    other.GetComponent<EnemyScript>().life -= power;
                    power -= 1;
                }
                else if (isEnchantment)
                {
                    other.GetComponent<EnemyScript>().inEnchant = true;
                    other.GetComponent<EnemyScript>().timeInDebuff = (float)power;
                    power -= 1;
                }
                power -= 1;
            }
            if (other.transform.CompareTag("Enemy Humanoid"))
            {
                if (isAttack)
                {
                    other.GetComponent<EnemyScript>().life -= power;
                    power -= 2;
                }
                else if (isLullaby)
                {
                    other.GetComponent<EnemyScript>().inLullaby = true;
                    other.GetComponent<EnemyScript>().timeInDebuff = (float) power;
                    power -= 1;
                }                
            }
            if (power <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
