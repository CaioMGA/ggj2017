﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWalk : MonoBehaviour {
	public float speed;
	public bool walking = false;
	Rigidbody2D rb;

	void Awake(){
		rb = GetComponent<Rigidbody2D>();
		stop();
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.A)){
			walk();
		}
		if(Input.GetKeyUp(KeyCode.A)){
			stop();
		}
	}

	public void walk(){
		if(!walking){
			rb.AddForce(new Vector2(-speed, 0));
			walking = true;
		}
	}

	public void stop(){
		rb.velocity = Vector2.zero;
		walking = false;
	}
}
