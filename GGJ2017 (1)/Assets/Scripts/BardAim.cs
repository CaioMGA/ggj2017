﻿using UnityEngine;

/// <summary>
/// Girar sprite quando o mouse mover para cima e para baixo na tela
/// </summary>
public class BardAim : MonoBehaviour {

    public float fov = 90;
    protected Transform thisTransform;

    void Start() {
        thisTransform = GetComponent<Transform>();
    }

    void Update() {
        //o (0,0) do mouse e' no canto inferior esquerdo 
        //o (0,0) do screen e' no canto superioir direito
        //para obter o valor do mouse igual ao de screen: (Mathf.Abs(Input.mousePosition.y - Screen.height))
        //para ter um valor que vai de -100 a 100 quando o mouse esta na parte superior ate' a parte inferior: ((mouse.y * 100) / Screen.height) - 50.0f) * 2
        //para limitar o valor de -100 a 100 ao valor de campo de visao: (valor * (fov / 2)) / 100
        float rot = ((((((Mathf.Abs(Input.mousePosition.y - Screen.height)) * 100) / Screen.height) - 50.0f) * 2) * (fov / 2)) / 100 ;
        //girar o transform
        thisTransform.localEulerAngles = new Vector3(thisTransform.localEulerAngles.x, thisTransform.localEulerAngles.y, -rot);
    }
}
