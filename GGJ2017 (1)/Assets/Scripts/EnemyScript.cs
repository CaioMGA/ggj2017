﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {
    public int TotalLife = 10;
    public int life;
    public bool inEnchant;
    public bool inLullaby;
    public int power = 1;
    public float timeInDebuff = 0;
    public bool inDebuff;
    Animator anim;
	//Collider2D collider;

    protected bool AlreadyAttacked = false;

	EnemyWalk enemy_walk;

    private void Start() {
        life = TotalLife;
        AlreadyAttacked = false;
        inEnchant = false;
        inLullaby = false;
        inDebuff = false;
        gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
    }

    void Awake(){
        enemy_walk = transform.GetComponent<EnemyWalk>();
        anim = transform.GetChild(0).GetComponent<Animator>();
	}

	public void disable(){ // send to staging 
		enemy_walk.stop();
		AlreadyAttacked = false;
		gameObject.SetActive(false);
	}

	public void deploy(){ // send enemy
		gameObject.SetActive(true); // does it make any sense?
		gameObject.GetComponent<BoxCollider2D>().enabled= true;
		enemy_walk.walk();
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }

	void OnTriggerEnter2D(Collider2D other){
		if(other.transform.CompareTag("Enemy Destroyer")){
			disable();
		}
        if (other.transform.CompareTag("Enemy Animal") && other.GetComponent<EnemyScript>().inEnchant)
        {
            life -= other.GetComponent<EnemyScript>().power;
        }
        if (!AlreadyAttacked) {
            if (other.transform.CompareTag("Bard")) {
                other.GetComponent<Health>().TakeDamage(power);
                AlreadyAttacked = true;
            }
        }
    }

    private void Update()
    {
        if (inEnchant && !inDebuff)
        {
            GetComponent<EnemyWalk>().stop();
            enemy_walk.speed *= -1;
            enemy_walk.walk();
            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            inDebuff = true;
        }
        if (inLullaby && !inDebuff)
        {
            GetComponent<EnemyWalk>().stop();
            inDebuff = true;
        }

        if (timeInDebuff > 0)
        {
            timeInDebuff -= Time.deltaTime;
        }
        else
        {
            inLullaby = false;
            if (inEnchant)
            {
                GetComponent<EnemyWalk>().stop();
                enemy_walk.speed *= -1;
                gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                inEnchant = false;
            }
            enemy_walk.walk();
            inDebuff = false;
        }

        if (life <= 0)
        {
            anim.SetTrigger("DEATH");
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            enemy_walk.stop();
			life = TotalLife;
        }
    }
}